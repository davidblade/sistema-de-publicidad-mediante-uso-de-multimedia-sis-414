import CreatePublication from '/imports/view/publications/CreatePublication'
import userPublications from '/imports/view/publications/userPublications'
import EditPublication from '../view/publications/EditPublication'

export default routes = [
    {
        path: "/dashboard/create-publications",
        component: CreatePublication,
        permission:"createpublication"
    },
    {
        path: "/dashboard/edit-publications",
        component: EditPublication,
        permission:"editpublication"
    },
    {
        path: "/dashboard/user-publications",
        component: userPublications,
        permission:"allpublicationuser"
    },
]