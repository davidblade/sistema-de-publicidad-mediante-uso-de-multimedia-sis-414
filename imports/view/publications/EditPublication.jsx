import React, { Component } from 'react'
import {publicationsClass} from '../../models/publications/class' //importamos la clase de nuestra collection o tabla 
import {categoryClass} from '../../models/catergory/class'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import DatePicker from "react-datepicker";
import FileUpload from '../../components/FileUpload'
import uploadFiles from '../../utils/upload'


class EditPublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:props.getpublication.title,//title->propiedad:null->valor
                description:props.getpublication.description,
                phone:props.getpublication.phone,
                price:props.getpublication.price,
                startDate:props.getpublication.startDate,
                endDate:props.getpublication.endDate,
                image:null,
                category:props.getpublication.idCategory._str
            },
            errors:{}
        }
    }
    validateForm=()=>{
        const {form} = this.state //obtener la propiedad form de this.state
        let errorsform = {}//objeto vacio
        let formsIsValid = true//verificar si el formulario es valido o no
        if(!form.title){//si titulo es vacio 
            formsIsValid = false
            errorsform['title'] = "El titulo no puede estar vacio"
        }
        if(!form.description){//si titulo es vacio 
            formsIsValid = false
            errorsform['description'] = "la description no puede estar vacio"
        }
        if(!form.phone){//si titulo es vacio 
            formsIsValid = false
            errorsform['phone'] = "el telefono no puede estar vacio"
        }
        if(!form.price){//si titulo es vacio 
            formsIsValid = false
            errorsform['price'] = "el precio no puede estar vacio"
        }
        if(!form.startDate){//si titulo es vacio 
            formsIsValid = false
            errorsform['startDate'] = "la fecha inicial no puede estar vacio"
        }
        if(!form.endDate){//si titulo es vacio 
            formsIsValid = false
            errorsform['endDate'] = "la fecha final no puede estar vacio"
        }
        this.setState({errors:errorsform}) //cambiamos el valor de la propiedad errors en this.state con los nuevos valores del metodo que estan en la variable errorsform
        return formsIsValid
    }

    editPublications=(e)=>{ // si va terner acceso a las valiables dentro del componente
        e.preventDefault()
        const {history} = this.props
        if(this.validateForm()){//si el formulario no tiene errores true pero si no sera falso
            //alert('enviando formulario')
            const editpublicacion = this.props.getpublication    
            const {form} = this.state//obtener la propiedad form de this.state
            form.price = parseInt(form.price)
            if(this.state.form.image){
                //////            
                const {form:{image}} = this.state //obtener la propiedad o campo image de form de this.state
                const uf = new uploadFiles(image.file,image.self) // clase uploadFiles que ya creamos nosotros en utils
                uf.newUpload(//newUpload es un metodo de la clase uploadFiles que recibe como parametro un callback una funcion
                    function(error,success){//esta funcion recibe como parametros una variable que tendra datos de error y otro que es succes donde enviamos datos exitosos
                    if(error){
                        console.log('*********************************')
                        console.log(error)
                        console.log('*********************************')
                    }else{
                            form.image = success._id
                            editpublicacion.callMethod('editPublication',form,(error,result)=>{ //newPublication esta declarado en la extencion de la clase  publicationsClass en el servidor
                                if(error){
                                    alert(error)
                                }else{
                                    alert(result)
                                    history.push('/dashboard/user-publications')
                                }
                            })       
                    }
                    }
                )
                //////
            }else{
                editpublicacion.callMethod('editPublication',form,(error,result)=>{ //newPublication esta declarado en la extencion de la clase  publicationsClass en el servidor
                    if(error){
                        alert(error)
                    }else{
                        alert(result)
                        history.push('/dashboard/user-publications')
                    }
                })  
            }
            return false
            
        }else{
           alert('el formulario tiene errores')
        }
    }
    changeTextInput = (e) =>{ //(e) esta llegando todas las propiedades que pueda tener el valor <input>
        const value = e.target.value // recuperar el valor que se esta escribiendo en el input
        const property = e.target.name// recupera la propiedad name del input

        //prevState => esta recuperando el valor anterior de this.state 
        //({form:{...prevState.form,title:value}}) => retornando el cambio hecho en la propiedad "form"
        // de "this.state" lo esta de la manera siguiente :
        // ""...prevState.form" esto esta sacando todas las propiedades de "form" (title:null,description:null,phone:null,price:null,startDate:null,endDate:null)
        //los siguientes valores despues de la coma (,) si la propieda ya existe en (...prevState.form) remplazara el valor que tenia  pero si no existe 
        // creara una nueva propiedad con nuevo valor
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [property]:value,//[property] la variable property lo ponemos dentro de corchetes para que sepa el metodo que esa propiedad va ser dinamica puede tener valores(title,description,price, etc ) del form
                }
            }
        ))

    }
    changeDateInput = (type,date) =>{
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [type]:date,//[type] la variable property lo ponemos dentro de corchetes para que sepa el metodo que esa propiedad va ser dinamica puede tener valores(title,description,price, etc ) del form
                }
            }
        ))
    }
    changeSelectInput =(e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    category:value,
                }
            }
        ))
    }
    changeFileInput =(data)=>{
        //console.log(data)
        const inputfile = data.file
        if(inputfile && inputfile[0]){
            console.log(inputfile[0])
            let reader = new FileReader()
            reader.onload = function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputfile[0])

            this.setState(prevState=>(
                {form:{
                        ...prevState.form,
                        image:data,
                    }
                }
            ))
        }
    }
    render() {
        const {errors,form} = this.state//recuperar la propiedad errors de this.state
        const {categorys,subcriptionCategory,getpublication,subcriptionPublication}= this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Editar Publicacion</h4>
                                    </div>
                                    {!subcriptionCategory.ready() && subcriptionPublication.ready()?
                                        <h1>Cargando!!!</h1>
                                    :
                                        <div className="card-body">
                                            <form onSubmit={this.editPublications} id="editPublication">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label>Titulo de publicacion</label>
                                                            <input type="text" className={errors.title?"form-control is-invalid":"form-control"} value={form.title} name={'title'} onChange={this.changeTextInput} autoComplete="off"/>
                                                            {errors.title?<div className="invalid-feedback">{errors.title}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Descripcion</label>
                                                            <textarea type="text" value={form.description}  className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"}  name={'description'} onChange={this.changeTextInput}>
                                                            </textarea>
                                                            {errors.description?<div className="invalid-feedback">{errors.description}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Numero de Telefono</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        <i className="fas fa-phone"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text" value={form.phone} className={errors.phone?"form-control phone-number is-invalid":"form-control phone-number"} name={'phone'} onChange={this.changeTextInput} autoComplete="off"/>
                                                                {errors.phone?<div className="invalid-feedback">{errors.phone}</div>:null}
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Precio</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        $
                                                                    </div>
                                                                </div>
                                                                <input type="text" value={form.price}  className={errors.price?"form-control currency is-invalid":"form-control currency"} name={'price'} onChange={this.changeTextInput} autoComplete="off"/>
                                                                {errors.price?<div className="invalid-feedback">{errors.price}</div>:null}
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Seleccione una Categoria</label>
                                                            <select className="form-control" value={form.category} onChange={this.changeSelectInput}>
                                                                <option>Seleccione una categoria</option>  
                                                                {
                                                                    categorys.map((category,key)=>{
                                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Archivo</label>
                                                            {/*<input type="file" className='form-contro'  name={'file'} onChange={this.changeFileInput}/>*/}
                                                            <FileUpload changeFileInput={this.changeFileInput}/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Publicacion</label>
                                                            {/*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.changeTextInput} autoComplete="off"/>*/}
                                                            <div>
                                                                <DatePicker selected={form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'startDate'} 
                                                                    onChange={date => {
                                                                        this.changeDateInput('startDate',date)
                                                                    }}
                                                                />
                                                            </div>
                                                            {errors.startDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.startDate}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Culminacion</label>
                                                            <div>
                                                                <DatePicker selected={form.endDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'endDate'} 
                                                                    onChange={date => {
                                                                        this.changeDateInput('endDate',date)
                                                                    }}
                                                                />
                                                            </div>
                                                            {errors.endDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.endDate}</div>:null}
                                                            {/*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'endDate'} onChange={this.changeTextInput} autoComplete="off"/>*/}
                                                        </div>
                                            
                                                    </div>
                                                    <div className="col-md-6 d-flex justify-content-center">
                                                        <div className="author-box-center">
                                                            <img alt="image" src={getpublication.urlfile}  id="previewimage" className="rounded-circle author-box-picture" style={{width: '100%',height:'500'}} />
                                                            <div className="clearfix" />
                                                            <div className="author-box-name">
                                                                
                                                            </div>
                                                            <div className="author-box-job d-flex justify-content-center">Vista Previa</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i> Primary</button>
                                            </form>
                                        </div>
                                    }
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const {location:{state:{publication}}} = props
    const subcriptionPublication = Meteor.subscribe('publications',{pub:publication},'getOnePublication')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const getpublication = publicationsClass.findOne()
    return {categorys,subcriptionCategory,getpublication,subcriptionPublication}
})(EditPublication)